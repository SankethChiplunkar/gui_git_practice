#include <iostream>
#include <string>

using namespace std;

int main()
{
    std::cout << "Hello World!" << std::endl;
    std::cout << "A new line is added to main." << std::endl;
    return 0;
}